import { LayoutSlotConfig } from '@spartacus/storefront';

export const E2HeaderConfig: LayoutSlotConfig = {
  header: {
    slots: [
      'NavigationBar',
    ],
  },
};
