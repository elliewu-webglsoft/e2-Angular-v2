import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { E2BannerComponent } from './componensts/e2-banner/e2-banner.component';
import { FooterComponent } from './componensts/footer/footer.component';
import { CmsConfig, ConfigModule } from '@spartacus/core';
import { OutletPosition, provideOutlet } from '@spartacus/storefront';



@NgModule({
  declarations: [
    // E2BannerComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,

  ],
  providers: [
    // provideOutlet({
    //   id: 'TopBanner',
    //   position: OutletPosition.AFTER,
    //   component: E2BannerComponent,
    // }),
    // provideOutlet({
    //   id: 'footer',
    //   position: OutletPosition.REPLACE,
    //   component: FooterComponent,
    // }),
  ]
})
export class HomepageModule { }
