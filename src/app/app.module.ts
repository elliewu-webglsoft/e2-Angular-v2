import { HttpClientModule } from "@angular/common/http";
import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpartacusModule } from './spartacus/spartacus.module';
import { HomepageModule } from './e2-lib/homepage.module';
import { OutletPosition, PageSlotModule, provideOutlet } from '@spartacus/storefront';
import { ConfigModule } from '@spartacus/core';
import { pageLayoutConfig } from './config/page-layout.config';
import { E2BannerComponent } from './e2-lib/componensts/e2-banner/e2-banner.component';

@NgModule({
  declarations: [
    AppComponent,
    E2BannerComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    SpartacusModule,
    BrowserTransferStateModule,
    HomepageModule,
    PageSlotModule,
    ConfigModule.withConfigFactory(pageLayoutConfig)
  ],
  providers: [
    provideOutlet({
      id: 'PreHeader',
      component: E2BannerComponent,
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
